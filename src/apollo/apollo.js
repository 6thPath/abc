import { ApolloClient } from 'apollo-boost'
import { InMemoryCache } from 'apollo-cache-inmemory'
import { HttpLink } from 'apollo-link-http'
import { split } from 'apollo-link'
import { WebSocketLink } from 'apollo-link-ws'
import { getMainDefinition } from 'apollo-utilities'
import { setContext } from 'apollo-link-context'

const httpLink = new HttpLink({
  uri: 'http://localhost:4000/graphql'
  // uri: 'http://qc2:8007/graphql'
})

const wsLink = new WebSocketLink({
  uri: `ws://localhost:4000/graphql`,
  // uri: `ws://qc2:8007/graphql`,
  options: {
    reconnect: true
  }
})

const link = split(
  ({ query }) => {
    const { kind, operation } = getMainDefinition(query)
    return kind === 'OperationDefinition' && operation === 'subscription'
  },
  wsLink,
  httpLink
)

export const Client = new ApolloClient({
  link: link,
  cache: new InMemoryCache()
})

export const AfterLoginClient = (token) => {
  let httpLink = new HttpLink({
    uri: 'http://localhost:4000/graphql'
    // uri: 'http://qc2:8007/graphql'
  })
  let wsLink = new WebSocketLink({
    uri: `ws://localhost:4000/graphql`,
    // uri: `ws://qc2:8007/graphql`,
    options: {
      reconnect: true
    }
  })
  let authLink = setContext((_, { headers }) => {
    return {
      headers: {
        ...headers,
        authorization: token ? `Bearer ${token}` : ''
      }
    }
  })
  const link = split(
    ({ query }) => {
      const { kind, operation } = getMainDefinition(query)
      return kind === 'OperationDefinition' && operation === 'subscription'
    },
    wsLink,
    httpLink
  )
  return new ApolloClient({
    link: authLink.concat(link),
    cache: new InMemoryCache()
  })
}

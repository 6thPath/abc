import gql from 'graphql-tag'
import { Client, AfterLoginClient } from '../apollo/apollo'

import jwt from 'jsonwebtoken'
import { SHA256 } from 'crypto-js'
const moment = require('moment')

// Settings
export const Settings = {
  state: {
    currentPage: '',
    roomList: [],
    isARModalVisible: false, // phrase 1
    inputRoomNumber: '',
    inputWidth: '',
    inputLength: '',
    inputName: '',
    messageForAR: '',
    isMessageVisible: false,
    isASModalVisible: false, // phrase 2
    tempRoom: [],
    tempEdge: 0,
    selectedRoomId: '',
    selectedRoomCode: 0,
    refresh: false
  },
  reducers: {
    changePage (state, currentPage) {
      return {...state, currentPage}
    },

    getRooms (state, roomList) {
      return {...state, roomList}
    },

    openARModal (state) {
      return {...state, isARModalVisible: true}
    },

    changeARModalInput (state, e) {
      if (e.target.name === 'inputWidth' || e.target.name === 'inputLength') {
        if (Number(e.target.value) > 20 || Number(e.target.value) < 0) {
          return {...state, messageForAR: 'Both width and length neither exceed 20 nor positive value.', isMessageVisible: true}
        }
        return {...state, [e.target.name]: Number(e.target.value), messageForAR: '', isMessageVisible: false}
      }
      return {...state, [e.target.name]: e.target.value, messageForAR: '', isMessageVisible: false}
    },

    showARModalMessage (state, messageForAR) {
      return {...state, messageForAR, isMessageVisible: true}
    },

    addRoom (state) {
      return {
        ...state,
        isARModalVisible: false,
        inputRoomNumber: '',
        inputWidth: '',
        inputLength: '',
        inputName: '',
        messageForAR: '',
        isMessageVisible: false
      }
    },

    removeRoom (state) {
      return {...state, refresh: !state.refresh}
    },

    closeARModal (state) {
      return {
        ...state,
        isARModalVisible: false,
        inputRoomNumber: '',
        inputWidth: '',
        inputLength: '',
        inputName: ''
      }
    },

    openASModal (state, selectedRoom) {
      let selectedRoomInfo = state.roomList.filter(room => room._id === selectedRoom)[0]
      let roomAsGrid = []
      for (let row = 0; row < selectedRoomInfo.length; row++) {
        roomAsGrid[row] = []
        for (let col = 0; col < selectedRoomInfo.width; col++) {
          let tempData = selectedRoomInfo.seats.filter(seat => seat.x === row && seat.y === col)[0]
          tempData
            ? roomAsGrid[row].push({
              _id: tempData._id,
              x: tempData.x,
              y: tempData.y,
              state: tempData.state
            })
            : roomAsGrid[row].push({
              _id: '',
              x: row,
              y: col,
              state: 0
            })
        }
      }
      let tempEdge = 96 / Number(selectedRoomInfo.width)
      let selectedRoomCode = selectedRoomInfo.code
      return {...state, isASModalVisible: true, tempRoom: roomAsGrid, tempEdge, selectedRoomId: selectedRoomInfo._id, selectedRoomCode}
    },

    changeSeatState (state, xny) {
      let placeAsArr = xny.split(' ')
      if (state.tempRoom[+(placeAsArr[0])][+(placeAsArr[1])].state < 2) {
        state.tempRoom[+(placeAsArr[0])][+(placeAsArr[1])].state += 1
      } else {
        state.tempRoom[+(placeAsArr[0])][+(placeAsArr[1])].state = 0
      }
      return {...state, refresh: !state.refresh}
    },

    submitSeat (state) {
      return {
        ...state,
        isASModalVisible: false,
        tempRoom: [],
        tempEdge: 0,
        selectedRoomId: '',
        selectedRoomCode: 0
      }
    },

    closeASModal (state) {
      return {
        ...state,
        isASModalVisible: false,
        tempRoom: [],
        tempEdge: 0,
        selectedRoomId: '',
        selectedRoomCode: 0
      }
    }
  },
  effects: (dispatch) => ({
    async asyncGetRooms (payload, rootState) {
      let result = await AfterLoginClient(rootState.Login.token).query({
        query: gql`{
          getRooms {
            _id code name width length isEnabled seats {
              _id code x y isEnabled roomId state
            }
          }
        }`,
        fetchPolicy: 'no-cache'
      })
      dispatch.Settings.getRooms(result.data.getRooms)
    },

    async asyncAddRoom (payload, rootState) {
      if (/\D/.test(rootState.Settings.inputRoomNumber) || rootState.Settings.inputRoomNumber === '') {
        dispatch.Settings.showARModalMessage(`Please input room-number to generate room-code`)
      } else if (rootState.Settings.inputName.trim() === '') {
        dispatch.Settings.showARModalMessage(`Please input room-name`)
      } else if (rootState.Settings.inputWidth === '' || rootState.Settings.inputWidth === 0) {
        dispatch.Settings.showARModalMessage(`Please input valid room-width`)
      } else if (rootState.Settings.inputLength === '' || rootState.Settings.inputLength === 0) {
        dispatch.Settings.showARModalMessage(`Please input valid room-length`)
      } else {
        let found = rootState.Settings.roomList.filter(roomCode => roomCode.code === `r${rootState.Settings.inputRoomNumber}`).length
        if (found > 0) {
          dispatch.Settings.showARModalMessage(`This room-code has been registered`)
        } else {
          await AfterLoginClient(rootState.Login.token).mutate({
            variables: {
              code: `r${rootState.Settings.inputRoomNumber}`,
              name: rootState.Settings.inputName.trim(),
              width: Number(rootState.Settings.inputWidth),
              length: Number(rootState.Settings.inputLength),
              isEnabled: true
            },
            mutation: gql`
              mutation addRoom ($code: String!, $name: String, $width: Int!, $length: Int!, $isEnabled: Boolean!) {
                addRoom(code: $code, name: $name, width: $width, length: $length, isEnabled: $isEnabled) {
                  _id code name width length seats {
                    code
                  }
                }
              }
            `
          })

          let result = await AfterLoginClient(rootState.Login.token).query({
            query: gql`{
              getRooms {
                _id code name width length isEnabled seats {
                  _id code x y isEnabled roomId state
                }
              }
            }`,
            fetchPolicy: 'no-cache'
          })
          dispatch.Settings.getRooms(result.data.getRooms)

          dispatch.Settings.addRoom()
        }
      }
    },

    async asyncRemoveRoom (payload, rootState) {
      await AfterLoginClient(rootState.Login.token).mutate({
        variables: {
          _id: payload
        },
        mutation: gql`
          mutation removeRoom ($_id: ID!) {
            removeRoom(_id: $_id) {
              _id
            }
          }
        `
      })

      let result = await AfterLoginClient(rootState.Login.token).query({
        query: gql`{
          getRooms {
            _id code name width length isEnabled seats {
              _id code x y isEnabled roomId state
            }
          }
        }`,
        fetchPolicy: 'no-cache'
      })
      dispatch.Settings.getRooms(result.data.getRooms)

      dispatch.Settings.removeRoom()
    },

    async asyncSubmitSeat (payload, rootState) {
      let newSeatList = []
      let addDecision = []
      let updateDecision = []
      let removeDecision = []
      rootState.Settings.tempRoom.map(row => row.map(col => newSeatList.push(col)))
      let currentSeatList = await rootState.Settings.roomList.filter(room => room._id === rootState.Settings.selectedRoomId)[0].seats
      // find update
      newSeatList.map(function (nSeat) {
        currentSeatList.map(function (oSeat) {
          if (oSeat._id === nSeat._id && nSeat.state !== oSeat.state && nSeat.state !== 0) {
            updateDecision.push(nSeat)
          } else if (oSeat._id === nSeat._id && nSeat.state === 0) {
            removeDecision.push(nSeat)
          }
          return oSeat
        })
        return nSeat
      })

      // find new
      newSeatList.map(nSeat => nSeat._id === '' && nSeat.state !== 0 && addDecision.push(nSeat))

      updateDecision.map(seat => AfterLoginClient(rootState.Login.token).mutate({
        variables: {
          _id: seat._id,
          state: seat.state
        },
        mutation: gql`
          mutation updateSeat ($_id: ID!, $state: Int) {
            updateSeat (_id: $_id, state: $state) {
              _id x y isEnabled roomId state
            }
          }
        `
      }))

      removeDecision.map(seat => AfterLoginClient(rootState.Login.token).mutate({
        variables: {
          _id: seat._id
        },
        mutation: gql`
          mutation removeSeat ($_id: ID!) {
            removeSeat (_id: $_id) {
              _id
            }
          }
        `
      }))

      await addDecision.map(seat => AfterLoginClient(rootState.Login.token).mutate({
        variables: {
          code: `${rootState.Settings.selectedRoomCode}_${seat.x}_${seat.y}`,
          x: seat.x,
          y: seat.y,
          state: seat.state,
          isEnabled: true,
          roomId: rootState.Settings.selectedRoomId
        },
        mutation: gql`
          mutation addSeat ($code: String!, $x: Int!, $y: Int!, $state: Int, $isEnabled: Boolean!, $roomId: String!) {
            addSeat (code: $code, x: $x, y: $y, state: $state, isEnabled: $isEnabled, roomId: $roomId) {
              code _id
            }
          }
        `
      }))

      let result = await AfterLoginClient(rootState.Login.token).query({
        query: gql`{
          getRooms {
            _id code name width length isEnabled seats {
              _id code x y isEnabled roomId state
            }
          }
        }`,
        fetchPolicy: 'no-cache'
      })
      dispatch.Settings.getRooms(result.data.getRooms)

      dispatch.Settings.submitSeat()
    }
  })
}

// Login
export const Login = {
  state: {
    inputUsername: '',
    inputPassword: '',
    isLoggedIn: false,
    userId: '',
    username: '',
    firstName: '',
    lastName: '',
    token: '',
    iconLoading: false,
    loginMessage: '',
    isLoginMessageVisible: false
  },
  reducers: {
    getChange (state, e) {
      return {
        ...state,
        [e.target.name]: e.target.value
      }
    },

    enterLoading (state) {
      return {...state, iconLoading: true}
    },

    showErrorMessage (state, loginMessage) {
      return {...state, iconLoading: false, loginMessage, isLoginMessageVisible: true}
    },

    signIn (state, userInfo) {
      return {
        ...state,
        isLoggedIn: true,
        username: userInfo[0].username,
        userId: userInfo[0]._id,
        firstName: userInfo[0].firstName,
        lastName: userInfo[0].lastName,
        token: userInfo[1],
        iconLoading: false,
        loginMessage: '',
        isLoginMessageVisible: false
      }
    },

    signOut (state) {
      return {
        ...state,
        isLoggedIn: false,
        userId: '',
        username: '',
        firstName: '',
        lastName: '',
        token: ''
      }
    }
  },
  effects: (dispatch) => ({
    async asyncSignIn (payload, rootState) {
      if (/\W/.test(rootState.Login.inputUsername) || rootState.Login.inputUsername === '') {
        dispatch.Login.showErrorMessage(`Username cannot be null or contains whitespace and special characters`)
      } else if (/\W/.test(rootState.Login.inputPassword) || rootState.Login.inputPassword === '') {
        dispatch.Login.showErrorMessage(`Password cannot be null or contains whitespace and special characters`)
      } else {
        dispatch.Login.enterLoading()
        let result = await Client.mutate({
          variables: {
            username: rootState.Login.inputUsername,
            password: SHA256(rootState.Login.inputPassword).toString()
          },
          mutation: gql`
            mutation login ($username: String!, $password: String!) {
              login (username: $username, password: $password) {
                token errors
              }
            }
          `
        })
        if (result.data.login.token === null) {
          dispatch.Login.showErrorMessage(result.data.login.errors)
        } else {
          let decodedData = jwt.decode(result.data.login.token)
          if (decodedData.sub.username === 'admin') {
            dispatch.Settings.changePage('setting')
          } else {
            dispatch.Settings.changePage('seatbooking')
          }
          dispatch.Login.signIn([decodedData.sub, result.data.login.token])
          dispatch.SeatBooking.getUserId(decodedData.sub._id)
        }
      }
    },

    asyncSignOut (payload, rootState) {
      dispatch.SeatBooking.refreshAll()
      dispatch.Login.signOut()
    }
  })
}

// Register
export const Register = {
  state: {
    inputUsername: '',
    inputPassword: '',
    inputFirstName: '',
    inputLastName: '',
    regMessage: '',
    isRegMessageVisible: false,
    successMessage: '',
    isSucMessageVisible: false
  },
  reducers: {
    getChange (state, e) {
      return {
        ...state,
        [e.target.name]: e.target.value
      }
    },

    showError (state, regMessage) {
      return {...state, regMessage, isRegMessageVisible: true}
    },

    completeSignUp (state, successMessage) {
      return {
        ...state,
        inputUsername: '',
        inputPassword: '',
        inputFirstName: '',
        inputLastName: '',
        regMessage: '',
        isRegMessageVisible: false,
        successMessage: `Success fully registered ${successMessage}`,
        isSucMessageVisible: true
      }
    }
  },
  effects: (dispatch) => ({
    async asyncSignUp (payload, rootState) {
      if (/\W/.test(rootState.Register.inputUsername) || rootState.Register.inputUsername === '') {
        dispatch.Register.showError(`Username cannot be null or contains whitespace and special characters`)
      } else if (/\W/.test(rootState.Register.inputPassword) || rootState.Register.inputPassword === '') {
        dispatch.Register.showError(`Password cannot be null or contains whitespace and special characters`)
      } else if (/\W/.test(rootState.Register.inputFirstName) || rootState.Register.inputFirstName === '') {
        dispatch.Register.showError(`FirstName cannot be null or contains whitespace and special characters`)
      } else if (/\W/.test(rootState.Register.inputLastName) || rootState.Register.inputLastName === '') {
        dispatch.Register.showError(`LastName cannot be null or contains whitespace and special characters`)
      } else {
        let result = await Client.mutate({
          variables: {
            username: rootState.Register.inputUsername,
            password: SHA256(rootState.Register.inputPassword).toString(),
            firstname: rootState.Register.inputFirstName,
            lastname: rootState.Register.inputLastName,
            isEnabled: true
          },
          mutation: gql`
          mutation addUser ($username: String!, $password: String, $firstname: String, $lastname: String, $isEnabled: Boolean! ) {
            addUser (username: $username, password: $password, firstname: $firstname, lastname: $lastname, isEnabled: $isEnabled ) {
              username
            }
          }
          `
        })
        dispatch.Register.completeSignUp(result.data.addUser.username)
      }
    }
  })
}

// Seat Booking
export const SeatBooking = {
  state: {
    roomList: [],
    selectedRoom: [],
    selectedRoomId: '',
    tempEdge: 0,
    userId: '',
    selectedRoomWidth: 0,
    selectedRoomLength: 0,
    selectedDay: 0,
    seatsList: [],
    scheduleList: [],
    isMessageVisible: false,
    refresh: false
  },
  reducers: {
    refreshAll (state) {
      return {
        ...state,
        roomList: [],
        selectedRoom: [],
        selectedRoomId: '',
        tempEdge: 0,
        userId: '',
        selectedRoomWidth: 0,
        selectedRoomLength: 0,
        selectedDay: 0,
        seatsList: [],
        scheduleList: [],
        isMessageVisible: false,
        refresh: false,
        isLoading: false
      }
    },

    refreshDate (state) {
      return {...state, selectedDay: 0, scheduleList: []}
    },

    onUnmount (state) {
      return {
        ...state,
        selectedRoom: [],
        selectedRoomId: '',
        tempEdge: 0,
        selectedRoomWidth: 0,
        selectedRoomLength: 0,
        selectedDay: 0,
        seatsList: [],
        scheduleList: []
      }
    },

    getUserId (state, userId) {
      return {...state, userId}
    },

    getSBRooms (state, roomList) {
      return {...state, roomList}
    },

    getSchedules (state, scheduleList) {
      return {...state, scheduleList}
    },

    getSubcription (state, payload) {
      if (payload.timestamp === null) {
        let tempSchedule = state.scheduleList.filter(schedule => schedule._id !== payload._id)
        return {...state, scheduleList: tempSchedule, isLoading: false}
      } else {
        if (moment(payload.timestamp * 1000).format(`DD-MM-YYYY`) === moment(state.selectedDay * 1000).format(`DD-MM-YYYY`)) {
          return {...state, scheduleList: [...state.scheduleList, payload], isLoading: false}
        } else {
          return {...state, isLoading: false}
        }
      }
    },

    getSelectedDay (state, selectedDay) {
      return {...state, selectedDay}
    },

    getSelectedRoomInfo (state, selectedRoom) {
      let selectedRoomInfo = state.roomList.filter(room => room._id === selectedRoom)[0]
      let roomAsGrid = []
      for (let row = 0; row < selectedRoomInfo.length; row++) {
        roomAsGrid[row] = []
        for (let col = 0; col < selectedRoomInfo.width; col++) {
          let tempData = selectedRoomInfo.seats.filter(seat => seat.x === row && seat.y === col)[0]
          tempData
            ? roomAsGrid[row].push({
              x: tempData.x,
              y: tempData.y,
              state: tempData.state
            })
            : roomAsGrid[row].push({
              x: row,
              y: col,
              state: 0
            })
        }
      }
      let selectedRoomId = selectedRoomInfo._id
      let tempEdge = 100 / Number(selectedRoomInfo.width)
      let seatsList = selectedRoomInfo.seats
      return {...state, selectedRoom: roomAsGrid, tempEdge, selectedRoomId, seatsList}
    },

    onChange (state, payload) {
      return {...state, refresh: !state.refresh}
    },

    setIsLoading (state) {
      return {...state, isLoading: true}
    }
  },
  effects: (dispatch) => ({
    async asyncGetRooms (payload, rootState) {
      let result = await AfterLoginClient(rootState.Login.token).query({
        query: gql`
          {
            getRooms {
              _id code name width length isEnabled seats {
                _id code x y isEnabled roomId state
              }
            }
          }
        `,
        fetchPolicy: 'no-cache'
      })
      dispatch.SeatBooking.getSBRooms(result.data.getRooms)
    },

    async asyncGetSchedules (payload, rootState) {
      dispatch.SeatBooking.getSelectedDay(payload)
      let result = await AfterLoginClient(rootState.Login.token).query({
        query: gql`
          {
            getSchedules {
              _id seatId userId timestamp user {
                firstname lastname
              }
            }
          }
        `,
        fetchPolicy: 'no-cache'
      })

      let scheduleList = []

      result.data.getSchedules.map(function (schedule) {
        if (moment(schedule.timestamp * 1000).format(`DD-MM-YYYY`) === moment(payload * 1000).format(`DD-MM-YYYY`)) {
          scheduleList.push(schedule)
        }
        return schedule
      })
      dispatch.SeatBooking.getSchedules(scheduleList)
    },

    async pickSchedule (payload, rootState) {
      dispatch.SeatBooking.setIsLoading()
      await AfterLoginClient(rootState.Login.token).mutate({
        variables: {
          seatId: payload[0],
          userId: rootState.SeatBooking.userId,
          timestamp: payload[1]
        },
        mutation: gql`
          mutation addSchedule ($seatId: String!, $userId: String!, $timestamp: Int!) {
            addSchedule (seatId : $seatId, userId: $userId, timestamp: $timestamp) {
              _id
            }
          }
        `
      })
    },

    async removeSchedule (payload, rootState) {
      dispatch.SeatBooking.setIsLoading()
      await AfterLoginClient(rootState.Login.token).mutate({
        variables: {
          _id: payload[0],
          seatId: payload[1]
        },
        mutation: gql`
          mutation deleteSchedule ($_id: ID!, $seatId: String!) {
            deleteSchedule (_id: $_id, seatId: $seatId) {
              _id
            }
          }
        `
      })
    },

    async asyncOnChange (payload, rootState) {
      await AfterLoginClient(rootState.Login.token).subscribe({
        query: gql`
        subscription schedulteUpdate {
          scheduleUpdate {
            _id seatId userId timestamp user {
              firstname lastname
            }
          }
        }
        `,
        fetchPolicy: 'no-cache'
      }).subscribe({
        async next (value) {
          dispatch.SeatBooking.getSubcription(value.data.scheduleUpdate)
        }
      })
    }
  })
}

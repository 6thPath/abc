  import React, { Component } from 'react'
import { connect } from 'react-redux'
import '../public/Setting.css'
import AddRoomModal from './AddRoomModal'
import AddSeatModal from './AddSeatModal'

import { Row, Col, Icon, Button, Badge, Divider } from 'antd'

class Setting extends Component {
  componentDidMount () {
    this.props.asyncGetRooms()
  }

  handleOpenARModal () {
    this.props.openARModal()
  }

  handleOpenASModal (e) {
    this.props.openASModal(e.target.value)
  }

  handleRemoveRoom (e) {
    this.props.asyncRemoveRoom(e.target.value)
  }

  render () {
    return (
      <div style={{width: '100%'}}>
        <AddRoomModal />
        <AddSeatModal />
        <Divider style={{marginBottom: 0}}>
          <Badge count={this.props.roomList.length} showZero overflowCount={10}>
            <h3 style={{margin: 6}}><Icon type='bars' /> Room List</h3>
          </Badge>
        </Divider>
        <Button type='primary' style={{margin: 10, marginTop: 0}} onClick={this.handleOpenARModal.bind(this)}><Icon type='plus-square-o' /> Create Room</Button>
        <Row type='flex' justify='space-around' align='middle' style={{textAlign: 'center'}}>
          <Col span={3} className='text-center'>Code</Col>
          <Col span={6} className='text-center'>Room</Col>
          <Col span={7} className='text-center'>Number of seat</Col>
          <Col span={8} className='text-center'>Action</Col>
        </Row>
        {this.props.roomList.map((room, i) =>
          <Row key={i} type='flex' justify='center' align='middle'>
            <Col span={3} className='text-center'>
              {room.code}
            </Col>
            <Col span={6} className='text-center'>
              {room.name}
            </Col>
            <Col span={7} className='text-center'>
              {room.seats.length}
            </Col>
            <Col span={8} className='text-center'>
              <Button.Group size={2}>
                <Button value={room._id} onClick={this.handleOpenASModal.bind(this)} type='primary' style={{margin: 6}}>
                  <Icon type='edit' />
                </Button>
                <Button value={room._id} onClick={this.handleRemoveRoom.bind(this)} type='danger' style={{margin: 6}}>
                  <Icon type='delete' />
                </Button>
              </Button.Group>
            </Col>
          </Row>
        )}
      </div>
    )
  }
}

function mapStateToProps (state) {
  return state.Settings
}

function mapDispatchToProps (dispatch) {
  return dispatch.Settings
}

export default connect(mapStateToProps, mapDispatchToProps)(Setting)

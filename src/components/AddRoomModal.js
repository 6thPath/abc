import React, { Component } from 'react'
import { connect } from 'react-redux'
import '../public/Setting.css'

import { Modal, Input, Icon, Divider, Row, Col } from 'antd'

class AddRoomModal extends Component {
  handleCloseARModal () {
    this.props.closeARModal()
  }

  handleChangeInput (e) {
    this.props.changeARModalInput(e)
  }

  handleSubmit () {
    this.props.asyncAddRoom()
  }

  render () {
    let roomAsGrid = []
    for (let row = 0; row < this.props.inputLength; row++) {
      roomAsGrid[row] = []
      for (let col = 0; col < this.props.inputWidth; col++) {
        roomAsGrid[row].push({x: row, y: col})
      }
    }
    let gridElementEdge = 96 / this.props.inputWidth
    return (
      <div>
        <Modal
          title='Add new Room'
          visible={this.props.isARModalVisible}
          onOk={this.handleSubmit.bind(this)}
          onCancel={this.handleCloseARModal.bind(this)}
          okText='Confirm'
          cancelText='Cancel'
          afterClose={this.handleCloseARModal.bind(this)}
          bodyStyle={{padding: '2%'}}
        >
          <Row type='flex' justify='center' align='middle'>
            <Col span={6} style={{textAlign: 'center'}}>
              <label htmlFor='arnumber'>Room code: </label>
              <Input
                id='arnumber'
                type='number'
                prefix={<Icon type='tags-o' style={{ color: 'rgba(0,0,0,.25)' }} />}
                name='inputRoomNumber'
                value={this.props.inputRoomNumber}
                onChange={this.handleChangeInput.bind(this)}
              />
            </Col>
            <Col span={8} style={{textAlign: 'center'}}>
              <label htmlFor='arname'>Room name: </label>
              <Input
                id='arname'
                prefix={<Icon type='tags-o' style={{ color: 'rgba(0,0,0,.25)' }} />}
                name='inputName'
                value={this.props.inputName}
                onChange={this.handleChangeInput.bind(this)}
              />
            </Col>
            <Col span={5} style={{textAlign: 'center'}}>
              <label htmlFor='arw'>Width: </label>
              <Input
                id='arw'
                type='number'
                prefix={<Icon type='right-square-o' style={{ color: 'rgba(0,0,0,.25)' }} />}
                name='inputWidth'
                value={this.props.inputWidth}
                onChange={this.handleChangeInput.bind(this)}
              />
            </Col>
            <Col span={5} style={{textAlign: 'center'}}>
              <label htmlFor='arl'>Length: </label>
              <Input
                id='arl'
                type='number'
                prefix={<Icon type='down-square-o' style={{ color: 'rgba(0,0,0,.25)' }} />}
                name='inputLength'
                value={this.props.inputLength}
                onChange={this.handleChangeInput.bind(this)}
              />
            </Col>
          </Row>
          <Row type='flex' justify='center' align='middle'>
            <Col span={24} style={{marginTop: 10}}>
              {this.props.isMessageVisible && <div className='armessage'>{this.props.messageForAR}</div>}
            </Col>
          </Row>
          <Divider>Room sketches</Divider>
          <div style={{
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            flexWrap: 'wrap',
            width: '96%'
          }}>
            {roomAsGrid.map((row, x) => row.map((col, y) =>
              <div
                key={y}
                style={{
                  width: gridElementEdge + '%',
                  paddingTop: gridElementEdge + '%',
                  background: 'rgba(0,0,0,.5)',
                  border: '1px solid white'
                }}
              >{}</div>
            ))}
          </div>
        </Modal>
      </div>
    )
  }
}

function mapStateToProps (state) {
  return state.Settings
}

function mapDispatchToProps (dispatch) {
  return dispatch.Settings
}

export default connect(mapStateToProps, mapDispatchToProps)(AddRoomModal)

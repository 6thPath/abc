import React, { Component } from 'react'
import { connect } from 'react-redux'
import { BrowserRouter as Router, Route, Switch, NavLink, Redirect } from 'react-router-dom'
import '../public/App.css'
import 'antd/dist/antd.css'
import Setting from './Setting'
import Login from './Login'
import Register from './Register'
// import Schedule from './Schedule'
import SeatBooking from './SeatBooking'

import { Menu, Icon, Button } from 'antd'

class App extends Component {
  handleSignOut () {
    this.props.lFunction.asyncSignOut()
  }

  handleChangePage (e) {
    this.props.sFunction.changePage(e.key)
  }

  render () {
    return (
      <Router>
        <div>
          {this.props.login.isLoggedIn &&
          <Menu
            onClick={this.handleChangePage.bind(this)}
            selectedKeys={[this.props.setting.currentPage]}
            mode='horizontal'
          >
            { this.props.login.username === 'admin' &&
            <Menu.Item key='setting'>
              <NavLink to='/setting'><Icon type='setting' />Setting</NavLink>
            </Menu.Item>}
            {/* { this.props.login.username === 'admin' &&
            <Menu.Item key='schedule'>
              <NavLink to='/schedule'><Icon type='appstore' />Schedule</NavLink>
            </Menu.Item>} */}
            <Menu.Item key='seatbooking'>
              <NavLink to='/seatbooking'><Icon type='calendar' />Seat Booking</NavLink>
            </Menu.Item>
            <Menu.Item style={{float: 'right'}}>
              <Button type='danger' block onClick={this.handleSignOut.bind(this)}><Icon type='logout' width='0.8em' />Sign Out</Button>
            </Menu.Item>
          </Menu>
          }
          <div className='center'>
            <Switch>
              <Route exact path='/' render={() => this.props.login.isLoggedIn
                ? <Redirect to='/setting' />
                : <Login />
              } />
              <Route exact path='/register' render={() => this.props.login.isLoggedIn
                ? <Redirect to='/setting' />
                : <Register />
              } />
              <Route exact path='/setting' render={() => this.props.login.isLoggedIn
                ? this.props.login.username === 'admin' ? <Setting /> : <Redirect to='/seatbooking' />
                : <Redirect to='/' />
              } />
              {/* <Route exact path='/schedule' render={() => this.props.login.isLoggedIn
                ? this.props.login.username === 'admin' ? <Schedule /> : <Redirect to='/seatbooking' />
                : <Redirect to='/' />
              } /> */}
              <Route exact path='/seatbooking' render={() => this.props.login.isLoggedIn
                ? <SeatBooking />
                : <Redirect to='/' />
              } />
            </Switch>
          </div>
        </div>
      </Router>
    )
  }
}

function mapStateToProps (state) {
  return {
    setting: state.Settings,
    login: state.Login
  }
}

function mapDispatchToProps (dispatch) {
  return {
    sFunction: dispatch.Settings,
    lFunction: dispatch.Login
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App)

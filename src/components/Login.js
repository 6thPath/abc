import React, { Component } from 'react'
import { connect } from 'react-redux'
// import { withCookies } from 'react-cookie'
import logo from '../public/dhslogo.png'
import '../public/Login.css'

import { NavLink } from 'react-router-dom'
import { Row, Col, Form, Icon, Input, Button, Alert } from 'antd'
import { HotKeys } from 'react-hotkeys'

class Login extends Component {
  handleChangeInput (e) {
    this.props.getChange(e)
  }

  handleSignIn () {
    // const { cookies } = this.props
    this.props.asyncSignIn()
  }

  render () {
    const enterHandler = {
      'enter': () => this.props.asyncSignIn()
    }
    return (
      <HotKeys handlers={enterHandler}>
        <div className='login-form center'>
          <Row type='flex' justify='center' align='middle'>
            <Col span={14}>
              <Form>
                <Form.Item>
                  <div className='center'>
                    <img src={logo} className='ant-col-10' alt='DHS logo' />
                  </div>
                </Form.Item>
                <Form.Item>
                  <label className='noselect' htmlFor='loginUsername'>
                    Username:
                    <Input
                      id='loginUsername'
                      prefix={<Icon type='user' style={{ color: 'rgba(0,0,0,.25)' }} />}
                      placeholder='Enter your username'
                      name='inputUsername'
                      value={this.props.inputUsername}
                      onChange={this.handleChangeInput.bind(this)}
                    />
                  </label>
                  <label className='noselect' htmlFor='loginPassword'>
                    Password:
                    <Input
                      id='loginPassword'
                      prefix={<Icon type='lock' style={{ color: 'rgba(0,0,0,.25)' }} />}
                      placeholder='Enter your password'
                      type='password'
                      name='inputPassword'
                      value={this.props.inputPassword}
                      onChange={this.handleChangeInput.bind(this)}
                    />
                  </label>
                </Form.Item>
                <Form.Item>
                  {this.props.isLoginMessageVisible && <Alert message={this.props.loginMessage} type='error' closable />}
                </Form.Item>
                <Form.Item>
                  <div className='center'>
                    <Button.Group>
                      <Button type='primary' icon='login' loading={this.props.iconLoading} onClick={this.handleSignIn.bind(this)}>
                        Sign In
                      </Button>
                      <Button type='dashed' >
                        <NavLink to='/register'>Sign Up <Icon type='up-circle-o' /></NavLink>
                      </Button>
                    </Button.Group>
                  </div>
                </Form.Item>
              </Form>
            </Col>
          </Row>
        </div>
      </HotKeys>
    )
  }
}

function mapStateToProps (state) {
  return state.Login
}

function mapDispatchToProps (dispatch) {
  return dispatch.Login
}

export default connect(mapStateToProps, mapDispatchToProps)(Login)

import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Row, Select, DatePicker, Avatar, Table, Tooltip, Alert, Icon, Button, message } from 'antd'
const moment = require('moment')

class SeatBooking extends Component {
  componentDidMount () {
    this.props.asyncGetRooms()
    this.props.asyncOnChange()
  }

  componentWillUnmount () {
    this.props.onUnmount()
  }

  dateToSelect (currentDate) {
    return currentDate && currentDate > moment().day(14)
  }

  highlightDatetoSelect (currentDate) {
    const style = {}
    if (currentDate < moment().day(14) && currentDate > moment().day(7)) {
      style.color = '#51d3ff'
      style.borderTop = '1px solid #0094c6'
      style.borderBottom = '1px solid #0094c6'
      style.borderRadius = '20%'
    } else if (currentDate > moment().day(14)) {
      style.userSelect = 'none'
      style.color = 'grey'
    } else {
      style.color = 'black'
    }
    return (
      <div style={style}>
        {currentDate.date()}
      </div>
    )
  }

  handleSelectDay (date, dateString) {
    if (/\d{2}[-]{1}\d{2}[-]{1}\d{4}/.test(dateString)) {
      let selected = dateString.split('-')
      const selectedDate = moment().set({
        'date': +selected[0],
        'month': +selected[1] - 1,
        'year': +selected[2],
        'hour': 0,
        'minute': 0,
        'second': 0
      }).unix()
      this.props.asyncGetSchedules(selectedDate)
    } else {
      this.props.refreshDate()
      message.error(`Please select a date!`)
    }
  }

  handleSelectRoom (roomId) {
    this.props.getSelectedRoomInfo(roomId)
  }

  handleBooking (e) {
    let payload = e.target.value.split('|')
    this.props.pickSchedule([payload[0], +this.props.selectedDay + +payload[1]])
  }

  handleRemove (e) {
    let payload = e.target.value.split('|')
    this.props.removeSchedule([payload[0], payload[1]])
  }

  render () {
    const that = this
    let data = []
    this.props.seatsList.map((seat, i) => seat.state === 1 && data.push({
      key: i,
      seat: `seat ${seat.x}_${seat.y}`,
      seatId: seat._id,
      am: 28800,
      amState: true,
      amId: '',
      amUserId: '',
      amUsername: '',
      pm: 46800,
      pmState: true,
      pmId: '',
      pmUserId: '',
      pmUsername: '',
      full: 61200,
      fullState: true,
      fullId: '',
      fullUserId: '',
      fullUsername: ''
    }))

    data.map(function (seat, index) {
      that.props.scheduleList.map(function (schedule) {
        if (seat.seatId === schedule.seatId) {
          if (+moment(schedule.timestamp * 1000).format('HH') === 8) {
            data[index].amState = false
            // data[index].pmState = true
            data[index].fullState = false
            data[index].amId = schedule._id
            data[index].amUserId = schedule.userId
            data[index].amUsername = `${schedule.user.firstname}${schedule.user.lastname}`
          } else if (+moment(schedule.timestamp * 1000).format('HH') === 13) {
            // data[index].amState = true
            data[index].pmState = false
            data[index].fullState = false
            data[index].pmId = schedule._id
            data[index].pmUserId = schedule.userId
            data[index].pmUsername = `${schedule.user.firstname}${schedule.user.lastname}`
          } else if (+moment(schedule.timestamp * 1000).format('HH') === 17) {
            data[index].amState = false
            data[index].pmState = false
            data[index].fullState = false
            data[index].fullId = schedule._id
            data[index].fullUserId = schedule.userId
            data[index].fullUsername = `${schedule.user.firstname}${schedule.user.lastname}`
            data[index].amUsername = `${schedule.user.firstname}${schedule.user.lastname}`
            data[index].pmUsername = `${schedule.user.firstname}${schedule.user.lastname}`
          }
        }
        return schedule
      })
      return seat
    })

    // console.log(data)

    const columns = [
      {title: 'Seat', dataIndex: 'seat', key: 'seat', width: 100, fixed: 'top'},
      {
        title: 'Session',
        children: [
          {
            title: 'AM',
            dataIndex: 'AM',
            key: 'am',
            width: 200,
            render: (text, row) => row.amState
              ? <Button loading={this.props.isLoading} icon='check-circle' type='primary' ghost value={`${row.seatId}|${row.am}`} onClick={this.handleBooking.bind(this)}>Pick</Button>
              : row.amUserId === that.props.userId
                ? <Button loading={this.props.isLoading} icon='close-circle' type='danger' ghost value={`${row.amId}|${row.seatId}`} onClick={this.handleRemove.bind(this)}>Undo</Button>
                : <p className='noselect'>{row.amUsername}</p>
          },
          {
            title: 'PM',
            dataIndex: 'PM',
            key: 'pm',
            width: 200,
            render: (text, row) => row.pmState
              ? <Button loading={this.props.isLoading} icon='check-circle' type='primary' ghost value={`${row.seatId}|${row.pm}`} onClick={this.handleBooking.bind(this)}>Pick</Button>
              : row.pmUserId === that.props.userId
                ? <Button loading={this.props.isLoading} icon='close-circle' type='danger' ghost value={`${row.pmId}|${row.seatId}`} onClick={this.handleRemove.bind(this)}>Undo</Button>
                : <p className='noselect'>{row.pmUsername}</p>
          },
          {
            title: 'Full',
            dataIndex: 'Full',
            key: 'full',
            width: 200,
            render: (text, row) => row.fullState
              ? <Button loading={this.props.isLoading} icon='check-circle' type='primary' ghost value={`${row.seatId}|${row.full}`} onClick={this.handleBooking.bind(this)}>Pick</Button>
              : row.fullUserId === that.props.userId
                ? <Button loading={this.props.isLoading} icon='close-circle' type='danger' ghost value={`${row.fullId}|${row.seatId}`} onClick={this.handleRemove.bind(this)}>Undo</Button>
                : row.amUsername === row.pmUsername ? <p className='noselect'>{row.amUsername}</p> : <Icon style={{color: '#f48d86'}} type='close-circle' theme='filled' />
          }
        ]
      }
    ]

    return (
      <div style={{margin: 10, height: '180vh'}}>
        {this.props.isMessageVisible && <Alert message={`Please select valid date!`} type='error' />}
        <div style={{background: 'rgba(0,0,0,.1)', padding: 10}}>
          <Row type='flex' justify='space-around' align='middle' >
            <Select defaultValue='Select a room' onChange={this.handleSelectRoom.bind(this)}>
              {this.props.roomList.map((room, i) => <Select.Option key={i} value={room._id}>{room.name}</Select.Option>)}
            </Select>
            <DatePicker
              format='DD-MM-YYYY'
              disabledDate={this.dateToSelect.bind(this)}
              dateRender={this.highlightDatetoSelect.bind(this)}
              onChange={this.handleSelectDay.bind(this)}
              // allowClear={false}
              placeholder='Select a date'
            />
          </Row>
          <div style={{
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-around',
            alignItems: 'center',
            margin: '1vh'
          }}>
            <Avatar size={20} style={{background: 'rgba(0, 0, 0, .3)'}} /> Not a seat
            <Avatar size={20} style={{background: 'rgba(0, 214, 53, 0.8)'}} /> Seat enabled
            <Avatar size={20} style={{background: 'rgba(255, 117, 117, 0.7)'}} /> Seat disabled
          </div>
        </div>
        {/* { map } */}
        <div className='responsive-map' style={{
          display: 'flex',
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
          flexWrap: 'wrap',
          width: '100%'
        }}>
          {this.props.selectedRoom.map((row, x) => row.map((col, y) =>
            col.state !== 0
              ? <Tooltip key={y} placement='topLeft' title={`x: ${col.x}, y:${col.y} seat:${col.x}_${col.y}`} arrowPointAtCenter>
                <div
                  className='noselect zoom'
                  style={{
                    width: this.props.tempEdge + '%',
                    paddingTop: this.props.tempEdge + '%',
                    background: col.state === 1 ? 'rgba(0, 214, 53, 0.8)' : col.state === 2 ? 'rgba(255, 117, 117, 0.7)' : 'rgba(0, 0, 0, .3)',
                    border: '1px solid white',
                    fontSize: this.props.tempEdge / 0.65
                  }}
                >{}</div>
              </Tooltip>
              : <div
                key={y}
                className='noselect'
                style={{
                  width: this.props.tempEdge + '%',
                  paddingTop: this.props.tempEdge + '%',
                  background: 'rgba(0, 0, 0, .3)',
                  border: '1px solid white',
                  fontSize: this.props.tempEdge / 0.65
                }}
              >{}</div>
          ))}
        </div>
        { (this.props.selectedDay !== 0 && this.props.selectedRoomId !== '' &&
        <Table
          columns={columns}
          dataSource={data}
          bordered
          size='middle'
        />) || <Alert message='Please select room and date to view seat-booking table' type='info' /> }
      </div>
    )
  }
}

function mapStateToProps (state) {
  return state.SeatBooking
}

function mapDispatchToProps (dispatch) {
  return dispatch.SeatBooking
}

export default connect(mapStateToProps, mapDispatchToProps)(SeatBooking)

import React, { Component } from 'react'
import { connect } from 'react-redux'
import '../public/Setting.css'

import { Modal, Avatar, Divider } from 'antd'

class AddSeatModal extends Component {
  handleAddSeat (e) {
    this.props.changeSeatState(e.target.value)
  }

  handleSubmit () {
    this.props.asyncSubmitSeat()
  }

  handleCloseModal () {
    this.props.closeASModal()
  }

  render () {
    return (
      <div>
        <Modal
          title='Add Seats'
          visible={this.props.isASModalVisible}
          onOk={this.handleSubmit.bind(this)}
          onCancel={this.handleCloseModal.bind(this)}
          okText='Done'
          cancelText='Cancel'
          afterClose={this.handleCloseModal.bind(this)}
          bodyStyle={{padding: '2%'}}
        >
          <div style={{
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-around',
            alignItems: 'center'
          }}>
            <Avatar size={20} style={{background: 'rgba(0, 0, 0, .5)'}} /> Not a seat
            <Avatar size={20} style={{background: 'rgba(0, 214, 53, 0.8)'}} /> Seat enabled
            <Avatar size={20} style={{background: 'rgba(255, 117, 117, 0.7)'}} /> Seat disabled
          </div>
          <Divider>Add seats to map below</Divider>
          <div style={{
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            flexWrap: 'wrap',
            width: '100%'
          }}>
            {this.props.tempRoom.map((row, x) => row.map((col, y) =>
              <button
                key={y}
                style={{
                  width: this.props.tempEdge + '%',
                  paddingTop: this.props.tempEdge + '%',
                  background: col.state === 1 ? 'rgba(0, 214, 53, 0.8)' : col.state === 2 ? 'rgba(255, 117, 117, 0.7)' : 'rgba(0, 0, 0, .3)',
                  border: '1px solid white',
                  outline: 'none'
                }}
                value={x + ' ' + y}
                onClick={this.handleAddSeat.bind(this)}
              >{}</button>
            ))}
          </div>
        </Modal>
      </div>
    )
  }
}

function mapStateToProps (state) {
  return state.Settings
}

function mapDispatchToProps (dispatch) {
  return dispatch.Settings
}

export default connect(mapStateToProps, mapDispatchToProps)(AddSeatModal)

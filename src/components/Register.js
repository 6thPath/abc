import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import logo from '../public/dhslogo.png'
import '../public/Login.css'

import { NavLink } from 'react-router-dom'
import { Row, Col, Form, Icon, Input, Button, Alert } from 'antd'

class Register extends Component {
  handleChange (e) {
    this.props.getChange(e)
  }

  handleSignUp () {
    this.props.asyncSignUp()
  }

  render () {
    return (
      <div className='register-form center'>
        <Row type='flex' justify='center' align='middle'>
          <Col span={18}>
            <Form>
              <Form.Item>
                <div className='center'>
                  <img src={logo} className='ant-col-10' alt='DHS logo' />
                </div>
              </Form.Item>
              <Form.Item>
                <label className='noselect' htmlFor='regUsername'>
                  Username:
                  <Input
                    id='regUsername'
                    prefix={<Icon type='user' style={{ color: 'rgba(0,0,0,.25)' }} />}
                    placeholder='Enter your username'
                    name='inputUsername'
                    value={this.props.inputUsername}
                    onChange={this.handleChange.bind(this)}
                  />
                </label>
                <label className='noselect' htmlFor='regPassword'>
                  Password:
                  <Input
                    id='regPassword'
                    prefix={<Icon type='lock' style={{ color: 'rgba(0,0,0,.25)' }} />}
                    placeholder='Enter your password'
                    type='password'
                    name='inputPassword'
                    value={this.props.inputPassword}
                    onChange={this.handleChange.bind(this)}
                  />
                </label>
                <label className='noselect' htmlFor='regFN'>
                  First name:
                  <Input
                    id='regFN'
                    prefix={<Icon type='tags-o' style={{ color: 'rgba(0,0,0,.25)' }} />}
                    placeholder='Enter your First name'
                    name='inputFirstName'
                    value={this.props.inputFirstName}
                    onChange={this.handleChange.bind(this)}
                  />
                </label>
                <label className='noselect' htmlFor='regLN'>
                  Last name:
                  <Input
                    id='regLN'
                    prefix={<Icon type='tags' style={{ color: 'rgba(0,0,0,.25)' }} />}
                    placeholder='Enter your Last name'
                    name='inputLastName'
                    value={this.props.inputLastName}
                    onChange={this.handleChange.bind(this)}
                  />
                </label>
              </Form.Item>
              <Form.Item>
                {this.props.isRegMessageVisible && <Alert showIcon message={this.props.regMessage} type='error' closable />}
                {this.props.isSucMessageVisible && <Alert showIcon message={this.props.successMessage} type='success' closable />}
              </Form.Item>
              <Form.Item>
                <div className='center'>
                  <Button.Group>
                    <Button type='primary' icon='up-circle-o' onClick={this.handleSignUp.bind(this)}>
                      Sign Up
                    </Button>
                    <Button type='dashed' >
                      <NavLink to='/'>Sign In <Icon type='login' /></NavLink>
                    </Button>
                  </Button.Group>
                </div>
              </Form.Item>
            </Form>
          </Col>
        </Row>
      </div>
    )
  }
}

Register.propTypes = {
  inputUsername: PropTypes.string.isRequired,
  inputPassword: PropTypes.string.isRequired,
  inputFirstName: PropTypes.string.isRequired,
  inputLastName: PropTypes.string.isRequired
}

function mapStateToProps (state) {
  return state.Register
}

function mapDispatchToProps (dispatch) {
  return dispatch.Register
}

export default connect(mapStateToProps, mapDispatchToProps)(Register)

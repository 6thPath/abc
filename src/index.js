import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import store from './rematch/store'

import { CookiesProvider } from 'react-cookie'

import { ApolloProvider } from 'react-apollo'
import { Client } from './apollo/apollo'

import App from './components/App'
import registerServiceWorker from './registerServiceWorker'

render(
  <CookiesProvider>
    <ApolloProvider client={Client} >
      <Provider store={store}>
        <App />
      </Provider>
    </ApolloProvider>
  </CookiesProvider>
  , document.getElementById('root'))
registerServiceWorker()
